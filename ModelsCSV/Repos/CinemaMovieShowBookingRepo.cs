﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TheMovies.Models;

namespace TheMovies.Repos
{
    public class CinemaMovieShowBookingRepo : IRepository<CinemaMovieShowBooking>
    {
        private List<CinemaMovieShowBooking> entries;

        public CinemaMovieShowBookingRepo()
        {
            entries = new List<CinemaMovieShowBooking>();
            LoadRepo();
        }

        public IEnumerable<CinemaMovieShowBooking> GetAll()
        {
            // Return a copy of the internal datastructure

            return entries.ToList();
        }

        public CinemaMovieShowBooking GetById(object id)
        {
            throw new NotImplementedException();
        }

        public void Add(CinemaMovieShowBooking obj)
        {
            bool isCinema = false;
            bool isMovie = false;
            bool isBooking = false;
            bool isShowTime = false;
            bool isLockedX1 = false;
            bool isLockedX2 = false;

            if (obj.CinemaName != default)
                isCinema = true;
            if (obj.MovieTitle != default)
                isMovie = true;
            if (obj.BookingMail != default)
                isBooking = true;
            if (obj.ShowDateTime != default)
                isShowTime = true;
            if ((isCinema && isMovie || isCinema && isBooking || isCinema && isShowTime) ||
                (isMovie && isBooking || isMovie && isShowTime) ||
                (isBooking && isShowTime))
                isLockedX1 = true;
            if (isCinema && isMovie && isBooking || isCinema && isMovie && isShowTime || 
                isCinema && isBooking && isShowTime || isMovie && isBooking && isShowTime)
                isLockedX2 = true;

            foreach (CinemaMovieShowBooking cmsb in entries)
            {
                if(!isLockedX1 && !isLockedX2)
                {
                    if(isCinema && CinemaExists(cmsb, obj) ||
                        isMovie && MovieExists(cmsb, obj) ||
                        isBooking && BookingExists(cmsb, obj) ||
                        isShowTime && ShowTimeExists(cmsb, obj))
                    {
                        throw new Exception();
                    }
                }
                else if(isLockedX1)
                {
                    if((isCinema && isMovie && CinemaExists(cmsb, obj) && MovieExists(cmsb, obj)) ||
                        (isCinema && isBooking && CinemaExists(cmsb, obj) && BookingExists(cmsb, obj)) ||
                        (isCinema && isShowTime && CinemaExists(cmsb, obj) && ShowTimeExists(cmsb, obj)) ||
                        (isMovie && isBooking && MovieExists(cmsb, obj) && BookingExists(cmsb, obj)) ||
                        (isMovie && isShowTime && MovieExists(cmsb, obj) && ShowTimeExists(cmsb, obj)) ||
                        (isBooking && isShowTime && BookingExists(cmsb, obj) && ShowTimeExists(cmsb, obj)))
                    {
                        throw new Exception();
                    }
                }
                else if(isLockedX2)
                {
                    if((isCinema && isMovie && isBooking && CinemaExists(cmsb, obj) && MovieExists(cmsb, obj) && BookingExists(cmsb, obj)) ||
                        (isCinema && isMovie && isShowTime && CinemaExists(cmsb, obj) && MovieExists(cmsb, obj) && ShowTimeExists(cmsb, obj)) ||
                        (isCinema && isBooking && isShowTime && CinemaExists(cmsb, obj) && BookingExists(cmsb, obj) && ShowTimeExists(cmsb, obj)) ||
                        (isMovie && isBooking && isShowTime && MovieExists(cmsb, obj) && BookingExists(cmsb, obj) && ShowTimeExists(cmsb, obj)))
                    {
                        throw new Exception();
                    }
                }
            }
            entries.Add(obj);
            SaveRepo();
        }

        public void Delete(CinemaMovieShowBooking obj)
        {
            bool isCinema = false;
            bool isMovie = false;
            bool isBooking = false;
            bool isShowTime = false;

            if (obj.CinemaName != default)
                isCinema = true;
            if (obj.MovieTitle != default)
                isMovie = true;
            if (obj.BookingMail != default)
                isBooking = true;
            if (obj.ShowDateTime != default)
                isShowTime = true;

            if (isCinema)
            {
                for (int i = 0; i < entries.Count; i++)
                {
                    if(entries[i].CinemaName == obj.CinemaName && entries[i].CinemaTown == obj.CinemaTown)
                    {
                        entries.Remove(entries[i]);
                        i--;
                    }
                }
            }
            else if(isMovie)
            {
                for (int i = 0; i < entries.Count; i++)
                {
                    if (entries[i].MovieTitle == obj.MovieTitle && entries[i].MovieGenre == obj.MovieGenre && entries[i].MovieDuration == obj.MovieDuration && entries[i].MovieDirector == obj.MovieDirector && entries[i].MovieReleaseDate == obj.MovieReleaseDate)
                    {
                        entries.Remove(entries[i]);
                        i--;
                    }
                }
            }
            else if(isBooking)
            {
                foreach (CinemaMovieShowBooking cmsb in entries)
                {
                    if (cmsb.BookingMail == obj.BookingMail && cmsb.BookingPhone == obj.BookingPhone)
                    {
                        entries.Remove(cmsb);
                        break;
                    }
                }
            }
            else if(isShowTime)
            {
                for (int i = 0; i < entries.Count; i++)
                {
                    if (entries[i].ShowDateTime == obj.ShowDateTime)
                    {
                        entries.Remove(entries[i]);
                        i--;
                    }
                }
            }
            SaveRepo();
        }

        public void Update(CinemaMovieShowBooking obj, CinemaMovieShowBooking newValues)
        {
            bool isCinema = false;
            bool isMovie = false;
            bool isBooking = false;
            bool isShowTime = false;

            if (obj.CinemaName != default)
                isCinema = true;
            if (obj.MovieTitle != default)
                isMovie = true;
            if (obj.BookingMail != default)
                isBooking = true;
            if (obj.ShowDateTime != default)
                isShowTime = true;

            if(isCinema)
            {
                for (int i = 0; i < entries.Count; i++)
                {
                    if (entries[i].CinemaName == obj.CinemaName && entries[i].CinemaTown == obj.CinemaTown)
                    {
                        entries[i].CinemaName = newValues.CinemaName;
                        entries[i].CinemaTown = newValues.CinemaTown;
                    }
                }
            }
            if(isMovie)
            {
                for (int i = 0; i < entries.Count; i++)
                {
                    if (entries[i].MovieTitle == obj.MovieTitle && entries[i].MovieGenre == obj.MovieGenre && entries[i].MovieDuration == obj.MovieDuration && entries[i].MovieDirector == obj.MovieDirector && entries[i].MovieReleaseDate == obj.MovieReleaseDate)
                    {
                        entries[i].MovieTitle = newValues.MovieTitle;
                        entries[i].MovieGenre = newValues.MovieGenre;
                        entries[i].MovieDuration = newValues.MovieDuration;
                        entries[i].MovieDirector = newValues.MovieDirector;
                        entries[i].MovieReleaseDate = newValues.MovieReleaseDate;
                    }
                }
            }
            if(isBooking)
            {
                foreach (CinemaMovieShowBooking cmsb in entries)
                {
                    if (cmsb.BookingMail == obj.BookingMail && cmsb.BookingPhone == obj.BookingPhone)
                    {
                        cmsb.BookingMail = newValues.BookingMail;
                        cmsb.BookingPhone = newValues.BookingPhone;
                    }
                }
            }
            if(isShowTime)
            {
                for (int i = 0; i < entries.Count; i++)
                {
                    if (entries[i].ShowDateTime == obj.ShowDateTime)
                    {
                        entries[i].ShowDateTime = newValues.ShowDateTime;
                    }
                }
            }
            SaveRepo();
        }

        private void LoadRepo()
        {
            string path = @"..\..\..\Ex41-TheMovies.csv";
            StreamReader sr = new StreamReader(path);

            string line = sr.ReadLine();
            line = sr.ReadLine();

            while(line != null)
            {
                CinemaMovieShowBooking cmsb = new CinemaMovieShowBooking();
                string[] words = line.Split(';');

                cmsb.CinemaName = words[0];
                cmsb.CinemaTown = words[1];
                cmsb.ShowDateTime = DateTime.Parse(words[2]);
                cmsb.MovieTitle = words[3];
                cmsb.MovieGenre = words[4];

                string[] numbers = words[5].Split(":");
                int durationInMinutes = int.Parse(numbers[0]) * 60;
                durationInMinutes += int.Parse(numbers[1]);
 
                cmsb.MovieDuration = durationInMinutes;
                cmsb.MovieDirector = words[6];
                cmsb.MovieReleaseDate = DateTime.Parse(words[7]);
                cmsb.BookingMail = words[8];
                cmsb.BookingPhone = words[9];

                entries.Add(cmsb);
                line = sr.ReadLine();
            }

            sr.Close();
        }
        private void SaveRepo()
        {
            string path = @"..\..\..\Ex41-TheMovies.csv";
            StreamWriter sw = new StreamWriter(path);

            string cmsbAsString = "Biograf";
            cmsbAsString += ";";
            cmsbAsString += "By";
            cmsbAsString += ";";
            cmsbAsString += "Forestillingstidspunkt";
            cmsbAsString += ";";
            cmsbAsString += "Filmtitel";
            cmsbAsString += ";";
            cmsbAsString += "Filmgenre";
            cmsbAsString += ";";
            cmsbAsString += "Filmvarighed";
            cmsbAsString += ";";
            cmsbAsString += "Filminstruktør";
            cmsbAsString += ";";
            cmsbAsString += "Premieredato";
            cmsbAsString += ";";
            cmsbAsString += "Bookingmail";
            cmsbAsString += ";";
            cmsbAsString += "Bookingtelefonnummer";

            sw.WriteLine(cmsbAsString);

            foreach (CinemaMovieShowBooking cmsb in entries)
            {
                cmsbAsString = cmsb.CinemaName;
                cmsbAsString += ";";
                cmsbAsString += cmsb.CinemaTown;
                cmsbAsString += ";";

                DateTime time = cmsb.ShowDateTime;
                cmsbAsString += time.ToString("yyyy-MM-dd HH':'mm");

                cmsbAsString += ";";
                cmsbAsString += cmsb.MovieTitle;
                cmsbAsString += ";";
                cmsbAsString += cmsb.MovieGenre;
                cmsbAsString += ";";

                int hours = cmsb.MovieDuration / 60;
                int minutes = cmsb.MovieDuration % 60;
                cmsbAsString += ("0" + hours + ":" + minutes);

                cmsbAsString += ";";
                cmsbAsString += cmsb.MovieDirector;
                cmsbAsString += ";";

                time = cmsb.MovieReleaseDate;
                cmsbAsString += time.ToString("yyyy-MM-dd");

                cmsbAsString += ";";
                cmsbAsString += cmsb.BookingMail;
                cmsbAsString += ";";
                cmsbAsString += cmsb.BookingPhone;

                sw.WriteLine(cmsbAsString);
            }

            sw.Close();
        }

        private bool CinemaExists(CinemaMovieShowBooking obj, CinemaMovieShowBooking newobj)
        {
            bool toReturn = false;
            if (obj.CinemaName == newobj.CinemaName && obj.CinemaTown == newobj.CinemaTown)
                toReturn = true;
            return toReturn;
        }
        private bool MovieExists(CinemaMovieShowBooking obj, CinemaMovieShowBooking newobj)
        {
            bool toReturn = false;
            if (obj.MovieTitle == newobj.MovieTitle && obj.MovieGenre == newobj.MovieGenre && obj.MovieDuration == newobj.MovieDuration && obj.MovieDirector == newobj.MovieDirector && obj.MovieReleaseDate == newobj.MovieReleaseDate)
                toReturn = true;
            return toReturn;
        }
        private bool BookingExists(CinemaMovieShowBooking obj, CinemaMovieShowBooking newobj)
        {
            bool toReturn = false;
            if (obj.BookingMail == newobj.BookingMail && obj.BookingPhone == newobj.BookingPhone)
                toReturn = true;
            return toReturn;
        }
        private bool ShowTimeExists(CinemaMovieShowBooking obj, CinemaMovieShowBooking newobj)
        {
            bool toReturn = false;
            if (obj.ShowDateTime == newobj.ShowDateTime)
                toReturn = true;
            return toReturn;
        }
    }
}
